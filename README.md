# @hinted/editorjs-custom-plugins

> Custom config for @editorjs

# Install

```bash
yarn add https://gitlab.com/hinted.me/editorjs-custom-plugins
```

## import default config from package and extend if needed

```javascript
import defaultConfig from '@hinted/editorjs-custom-plugins';
```

## Also you may import custom plugins

```javascript
import {
  VideoLink,
  ScenarioLink,
  ScenarioStopLink,
  UploadVideo,
} from '@hinted/editorjs-custom-plugins';
```

## Extend config for custom plugins

```javascript
const config = {
  ...defaultConfig,
  tools: {
    ...defaultConfig.tools,
    header: {
      class: defaultConfig.tools.header.class,
      config: {
        placeholder: `Title...`,
        levels: [1, 2, 3, 4, 5, 6],
        defaultLevel: 2,
      },
    },

    image: {
      class: defaultConfig.tools.image.class,
      config: {
        uploader: {
          uploadByFile: () => {},
          uploadByUrl: () => {},
        },
      },
    },

    video: {
      class: defaultConfig.tools.video.class,
      config: {
        uploadVideo: () => {},
      },
    },

    scenarioLink: {
      class: defaultConfig.tools.scenarioLink.class,
      config: {
        searchFunction: () => {},
      },
    },
  },
};
```

## Add package to transpileDependencies to vue.config.js

```javascript
  transpileDependencies: [
    '@hinted/editorjs-custom-plugins',
  ],
```

## Import css from custom plugins to project

```bash
  @import '@hinted/editorjs-custom-plugins/libs/scenarioLink/scenarioLink.css';
  @import '@hinted/editorjs-custom-plugins/libs/scenarioStopLink/scenarioStopLink.css';
  @import '@hinted/editorjs-custom-plugins/libs/videoLink/videoLink.css';
  @import '@hinted/editorjs-custom-plugins/libs/uploadVideo/uploadVideo.css';
  @import '@hinted/editorjs-custom-plugins/alignBlocks/style.css';
  @import '@hinted/editorjs-custom-plugins/libs/videoManager/videoManager.css';
  @import '@hinted/editorjs-custom-plugins/libs/uploadVideoFromLink/uploadVideoFromLink.css';
```

### list of default includes plugins

- Header
- List
- Paragraph
- Embed
- Quote
- Delimiter
- Underline
- Image
- FontSize(editorjs-inline-font-size-tool)
- TextColor(@rogierw/editor-js-text-color)
- VideoManager
- uploadVideoFromLink
