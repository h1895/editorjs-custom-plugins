class SelectionUtils {
  constructor() {
    this.instance = null;
    this.selection = null;
    this.savedSelectionRange = null;
    this.isFakeBackgroundEnabled = false;
    this.commandBackground = 'backColor';
    this.commandRemoveFormat = 'removeFormat';
  }

  /**
   * Return first range
   * @return {Range|null}
   */
  static get range() {
    const selection = window.getSelection();

    return selection && selection.rangeCount ? selection.getRangeAt(0) : null;
  }

  /**
   * Returns selected text as String
   * @returns {string}
   */
  static get text() {
    return window.getSelection ? window.getSelection().toString() : '';
  }

  /**
   * Returns window SelectionUtils
   * {@link https://developer.mozilla.org/ru/docs/Web/API/Window/getSelection}
   * @return {Selection}
   */
  static get() {
    return window.getSelection();
  }

  /**
   * Removes fake background
   */
  removeFakeBackground() {
    if (!this.isFakeBackgroundEnabled) {
      return;
    }

    this.isFakeBackgroundEnabled = false;
    document.execCommand(this.commandRemoveFormat);
  }

  /**
   * Sets fake background
   */
  setFakeBackground() {
    document.execCommand(this.commandBackground, false, '#d4ecff');
    this.isFakeBackgroundEnabled = true;
  }

  /**
   * Save SelectionUtils's range
   */
  save() {
    this.savedSelectionRange = SelectionUtils.range;
  }

  /**
   * Restore saved SelectionUtils's range
   */
  restore() {
    if (!this.savedSelectionRange) {
      return;
    }

    const sel = window.getSelection();

    sel.removeAllRanges();
    sel.addRange(this.savedSelectionRange);
  }

  /**
   * Clears saved selection
   */
  clearSaved() {
    this.savedSelectionRange = null;
  }

  /**
   * Collapse current selection
   */
  collapseToEnd() {
    const sel = window.getSelection();
    const range = document.createRange();

    range.selectNodeContents(sel.focusNode);
    range.collapse(false);
    sel.removeAllRanges();
    sel.addRange(range);
  }

  findParentTag(tagName, className, searchDepth = 10) {
    const selection = window.getSelection();
    let parentTag = null;

    /**
     * If selection is missing or no anchorNode or focusNode were found then return null
     */
    if (!selection || !selection.anchorNode || !selection.focusNode) {
      return null;
    }

    /**
     * Define Nodes for start and end of selection
     */
    const boundNodes = [
      /** the Node in which the selection begins */
      selection.anchorNode,
      /** the Node in which the selection ends */
      selection.focusNode,
    ];

    /**
     * For each selection parent Nodes we try to find target tag [with target class name]
     * It would be saved in parentTag variable
     */
    boundNodes.forEach((parent) => {
      /** Reset tags limit */
      let searchDepthIterable = searchDepth;

      while (searchDepthIterable > 0 && parent.parentNode) {
        /**
         * Check tag's name
         */
        if (parent.tagName === tagName) {
          /**
           * Save the result
           */
          parentTag = parent;

          /**
           * Optional additional check for class-name mismatching
           */
          if (
            className &&
            parent.classList &&
            !parent.classList.contains(className)
          ) {
            parentTag = null;
          }

          /**
           * If we have found required tag with class then go out from the cycle
           */
          if (parentTag) {
            break;
          }
        }

        /**
         * Target tag was not found. Go up to the parent and check it
         */
        parent = parent.parentNode;
        searchDepthIterable--;
      }
    });

    /**
     * Return found tag or null
     */
    return parentTag;
  }

  /**
   * Expands selection range to the passed parent node
   *
   * @param {HTMLElement} element - element which contents should be selcted
   */
  expandToTag(element) {
    const selection = window.getSelection();

    selection.removeAllRanges();
    const range = document.createRange();

    range.selectNodeContents(element);
    selection.addRange(range);
  }
}

class ScenarioStopLink {
  constructor({ data, api, config }) {
    this.config = config;
    this.api = api;
    this.data = data;
    this.state = false;
    this.selection = new SelectionUtils();
    this.icon =
      '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="stop" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-stop fa-w-14 fa-3x"><path fill="currentColor" d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48z" class=""></path></svg>';
    this.mainWrapper = document.createElement('div');
    this.reset = window;

    this.button = null;
    this.CSS = {
      button: 'ce-inline-tool',
      buttonModifier: 'ce-inline-tool--link',
      buttonActive: 'ce-inline-tool--active',
      buttonUnlink: 'ce-inline-tool--unlink',
    };
  }

  static get template() {
    // eslint-disable-next-line no-template-curly-in-string
    const element = document.createElement('scenario');
    element.setAttribute('hinted-click', 'onScenarioStopLinkClick');
    element.classList.add('hinted-scenario-link__stop-link');
    return element;
  }

  static get isInline() {
    return true;
  }

  static getRenderedHtml({ text }) {
    const element = ScenarioStopLink.template;
    element.innerHTML = text;
    element.style.textDecoration = 'underline';
    element.style.fontWeight = 'bold';
    return element;
  }

  static get sanitize() {
    return {
      scenario: true, // save all attributes for all 'scenario' tags
      svg: true,
    };
  }

  render() {
    this.button = document.createElement('button');
    this.button.innerHTML = this.icon; // `${this.icon} stop`;
    this.button.classList.add(this.CSS.button, this.CSS.buttonModifier);
    return this.button;
  }

  surround(range) {
    if (this.state) {
      // If highlights is already applied, remove tag
      this.unwrap();

      return;
    }

    this.wrap(range);
  }

  wrap(range) {
    const selectedText = `${range.extractContents().textContent}`;
    const element = ScenarioStopLink.getRenderedHtml({ text: selectedText });
    range.insertNode(element);
    this.selection.expandToTag(element);
  }

  unwrap() {
    const termWrapper = this.selection.findParentTag('SCENARIO');

    const sel = this.reset.getSelection();
    const range = sel.getRangeAt(0);
    const unwrappedContent = range.extractContents();
    termWrapper.parentNode.removeChild(termWrapper);
    range.insertNode(unwrappedContent);
    sel.removeAllRanges();
    sel.addRange(range);
  }

  checkState() {
    const anchorTag = this.selection.findParentTag('SCENARIO');
    const isAnchorTag =
      anchorTag &&
      !this.selection.findParentTag('SCENARIO').getAttribute('hinted-link');

    if (isAnchorTag) {
      this.button.classList.add(this.CSS.buttonUnlink);
      this.button.classList.add(this.CSS.buttonActive);
      this.selection.save();
    } else {
      this.button.classList.remove(this.CSS.buttonUnlink);
      this.button.classList.remove(this.CSS.buttonActive);
    }

    this.state = !!anchorTag;
  }
}

// comment line below if you run it from example.html (debug only)
export default ScenarioStopLink;
