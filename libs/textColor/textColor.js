class SelectionUtils {
  constructor() {
    this.instance = null;
    this.selection = null;
    this.savedSelectionRange = null;
    this.isFakeBackgroundEnabled = false;
    this.commandBackground = 'backColor';
    this.commandRemoveFormat = 'removeFormat';
  }

  /**
   * Return first range
   * @return {Range|null}
   */
  static get range() {
    const selection = window.getSelection();

    return selection && selection.rangeCount ? selection.getRangeAt(0) : null;
  }

  /**
   * Returns selected text as String
   * @returns {string}
   */
  static get text() {
    return window.getSelection ? window.getSelection().toString() : '';
  }

  /**
   * Returns window SelectionUtils
   * {@link https://developer.mozilla.org/ru/docs/Web/API/Window/getSelection}
   * @return {Selection}
   */
  static get() {
    return window.getSelection();
  }

  /**
   * Removes fake background
   */
  removeFakeBackground() {
    if (!this.isFakeBackgroundEnabled) {
      return;
    }

    this.isFakeBackgroundEnabled = false;
    document.execCommand(this.commandRemoveFormat);
  }

  /**
   * Sets fake background
   */
  setFakeBackground() {
    document.execCommand(this.commandBackground, false, '#d4ecff');
    this.isFakeBackgroundEnabled = true;
  }

  /**
   * Save SelectionUtils's range
   */
  save() {
    this.savedSelectionRange = SelectionUtils.range;
  }

  /**
   * Restore saved SelectionUtils's range
   */
  restore() {
    if (!this.savedSelectionRange) {
      return;
    }

    const sel = window.getSelection();

    sel.removeAllRanges();
    sel.addRange(this.savedSelectionRange);
  }

  /**
   * Clears saved selection
   */
  clearSaved() {
    this.savedSelectionRange = null;
  }

  /**
   * Collapse current selection
   */
  collapseToEnd() {
    const sel = window.getSelection();
    const range = document.createRange();

    range.selectNodeContents(sel.focusNode);
    range.collapse(false);
    sel.removeAllRanges();
    sel.addRange(range);
  }

  findParentTag(tagName, className, searchDepth = 10) {
    const selection = window.getSelection();
    let parentTag = null;

    /**
     * If selection is missing or no anchorNode or focusNode were found then return null
     */
    if (!selection || !selection.anchorNode || !selection.focusNode) {
      return null;
    }

    /**
     * Define Nodes for start and end of selection
     */
    const boundNodes = [
      /** the Node in which the selection begins */
      selection.anchorNode,
      /** the Node in which the selection ends */
      selection.focusNode,
    ];

    /**
     * For each selection parent Nodes we try to find target tag [with target class name]
     * It would be saved in parentTag variable
     */
    boundNodes.forEach((parent) => {
      /** Reset tags limit */
      let searchDepthIterable = searchDepth;

      while (searchDepthIterable > 0 && parent.parentNode) {
        /**
         * Check tag's name
         */
        if (parent.tagName === tagName) {
          /**
           * Save the result
           */
          parentTag = parent;

          /**
           * Optional additional check for class-name mismatching
           */
          if (
            className &&
            parent.classList &&
            !parent.classList.contains(className)
          ) {
            parentTag = null;
          }

          /**
           * If we have found required tag with class then go out from the cycle
           */
          if (parentTag) {
            break;
          }
        }

        /**
         * Target tag was not found. Go up to the parent and check it
         */
        parent = parent.parentNode;
        searchDepthIterable--;
      }
    });

    /**
     * Return found tag or null
     */
    return parentTag;
  }

  /**
   * Expands selection range to the passed parent node
   *
   * @param {HTMLElement} element - element which contents should be selcted
   */
  expandToTag(element) {
    const selection = window.getSelection();

    selection.removeAllRanges();
    const range = document.createRange();

    range.selectNodeContents(element);
    selection.addRange(range);
  }
}

const convertToHex = (color) => {
  const rgb = color.match(/(\d+)/g);

  let hexr = parseInt(rgb[0], 10).toString(16);
  let hexg = parseInt(rgb[1], 10).toString(16);
  let hexb = parseInt(rgb[2], 10).toString(16);

  hexr = hexr.length === 1 ? `0${hexr}` : hexr;
  hexg = hexg.length === 1 ? `0${hexg}` : hexg;
  hexb = hexb.length === 1 ? `0${hexb}` : hexb;

  return `#${hexr}${hexg}${hexb}`;
};

class TextColor {
  constructor({ api }) {
    this.api = api;

    this.button = null;
    this.status = false;

    this.tag = 'FONT';
    this.class = 'cdx-text-color';
    this.defaultColor = '#f50400';
    this.element = null;
    this.range = null;

    this.selection = new SelectionUtils();
    this.reset = window;
  }

  static get sanitize() {
    return {
      font: {
        style: true,
        class: true,
        size: true,
      },
    };
  }

  static get isInline() {
    return true;
  }

  get state() {
    return this.status;
  }

  set state(state) {
    this.status = state;

    this.button.classList.toggle(this.api.styles.inlineToolButtonActive, state);
  }

  static getTemplate({ text, color }) {
    const element = document.createElement('font');
    element.style.color = color;
    element.classList.add('cdx-text-color');
    element.appendChild(text);
    return element;
  }

  render() {
    this.button = document.createElement('button');

    this.button.type = 'button';
    this.button.innerHTML =
      '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="palette" class="svg-inline--fa fa-palette fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M204.3 5C104.9 24.4 24.8 104.3 5.2 203.4c-37 187 131.7 326.4 258.8 306.7 41.2-6.4 61.4-54.6 42.5-91.7-23.1-45.4 9.9-98.4 60.9-98.4h79.7c35.8 0 64.8-29.6 64.9-65.3C511.5 97.1 368.1-26.9 204.3 5zM96 320c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm32-128c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128-64c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128 64c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path></svg>';
    this.button.classList.add(this.api.styles.inlineToolButton);

    return this.button;
  }

  surround(range) {
    if (this.state) {
      this.unwrap(range);

      return;
    }

    this.wrap(range);
  }

  wrap(range) {
    const selectedText = range.extractContents();
    const element = TextColor.getTemplate({
      text: selectedText,
      color: this.colorPicker.value,
    });
    range.insertNode(element);
    this.selection.expandToTag(element);

    this.element = element;
  }

  unwrap() {
    const span = this.selection.findParentTag(this.tag);
    this.selection.expandToTag(span);

    const sel = this.reset.getSelection();
    const range = sel.getRangeAt(0);
    const unwrappedContent = range.extractContents();
    span.parentNode.removeChild(span);
    range.insertNode(unwrappedContent);
    sel.removeAllRanges();
    sel.addRange(range);
  }

  renderActions() {
    this.colorPicker = document.createElement('input');

    this.colorPicker.type = 'color';
    this.colorPicker.value = this.defaultColor;
    this.colorPicker.hidden = true;
    this.colorPicker.style.width = '0px';
    this.colorPicker.style.height = '0px';
    this.colorPicker.style.visibility = 'hidden';
    return this.colorPicker;
  }

  changeColor() {
    this.element.style.color = this.colorPicker.value;
    this.selection.expandToTag(this.element);
  }

  showActions() {
    const { color } = this.element.style;

    this.colorPicker.value = color ? convertToHex(color) : this.defaultColor;
    this.changeColorMethod = this.changeColor.bind(this);
    this.colorPicker.addEventListener('input', this.changeColorMethod);

    this.colorPicker.hidden = false;
  }

  hideActions() {
    this.colorPicker.removeEventListener('input', this.changeColorMethod);
    this.colorPicker.hidden = true;
  }

  checkState() {
    this.element = this.selection.findParentTag(this.tag);

    this.state = this.element
      ? !!(this.element && this.element.getAttribute('style'))
      : false;

    if (this.state) {
      this.showActions();
      window.requestAnimationFrame(() => this.colorPicker.click());
    } else {
      this.hideActions();
    }
  }
}

export default TextColor;
