import UploadVideo from '../uploadVideo/uploadVideo';
import UploadVideoFromLink from '../uploadVideoFromLink/uploadVideoFromLink';

class VideoManager {
  constructor({ data, api, config }) {
    this.data = data;
    this.api = api;
    console.log(api.i18n);
    this.i18n = api.i18n;
    this.config = config;
    this.wrapper = null;
    this.itemLink = null;
    this.itemUpload = null;
    this.videoUrl = null;
  }

  static get toolbox() {
    return {
      title: 'VideoManager',
      icon:
        '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="24" height="24" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">\n' +
        '<g>\n' +
        '\t<g>\n' +
        '\t\t<g>\n' +
        '\t\t\t<path d="M469.333,119.467H384c-14.114,0-25.6-11.486-25.6-25.6V51.2c0-4.71-3.814-8.533-8.533-8.533     c-4.719,0-8.533,3.823-8.533,8.533v42.667c0,23.526,19.14,42.667,42.667,42.667h76.8v332.8c0,14.114-11.486,25.6-25.6,25.6H76.8     c-14.114,0-25.6-11.486-25.6-25.6V42.667c0-14.114,11.486-25.6,25.6-25.6h269.534L429.167,99.9c3.337,3.337,8.73,3.337,12.066,0     c3.336-3.336,3.336-8.73,0-12.066L355.9,2.5c-1.596-1.604-3.763-2.5-6.033-2.5H76.8C53.274,0,34.133,19.14,34.133,42.667v426.667     C34.133,492.86,53.274,512,76.8,512h358.4c23.526,0,42.667-19.14,42.667-42.667V128     C477.867,123.29,474.052,119.467,469.333,119.467z"/>\n' +
        '\t\t\t<path d="M298.667,426.667h51.2c4.719,0,8.533-3.823,8.533-8.533v-204.8c0-4.71-3.814-8.533-8.533-8.533H162.133     c-4.719,0-8.533,3.823-8.533,8.533v204.8c0,4.71,3.814,8.533,8.533,8.533h102.4c4.719,0,8.533-3.823,8.533-8.533     s-3.814-8.533-8.533-8.533h-42.667v-85.333h68.267v93.867C290.133,422.844,293.948,426.667,298.667,426.667z M307.2,221.867     h34.133V256H307.2V221.867z M307.2,273.067h34.133V307.2H307.2V273.067z M307.2,324.267h34.133V358.4H307.2V324.267z      M307.2,375.467h34.133V409.6H307.2V375.467z M204.8,409.6h-34.133v-34.133H204.8V409.6z M204.8,358.4h-34.133v-34.133H204.8     V358.4z M204.8,307.2h-34.133v-34.133H204.8V307.2z M204.8,256h-34.133v-34.133H204.8V256z M221.867,307.2v-85.333h68.267V307.2     H221.867z"/>\n' +
        '\t\t</g>\n' +
        '\t</g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '<g>\n' +
        '</g>\n' +
        '</svg>',
    };
  }

  pastVideo() {
    const youtube = this.data.src.match(
      /(?:youtube\.com\/(?:[^\\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\\/ ]{11})/i
    );

    const iframe = document.createElement('iframe');
    iframe.setAttribute('class', 'upload-video-from-link__video');

    if (youtube) {
      iframe.setAttribute(
        'src',
        `https://www.youtube.com/embed/${youtube[1]}?autoplay=1`
      );
      this.wrapper.innerHTML = '';
      this.wrapper.appendChild(iframe);
      return this.wrapper;
    }

    const vimeo = this.data.src.match(
      /(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\\-]+)?/i
    );

    if (vimeo) {
      iframe.setAttribute(
        'src',
        `https://player.vimeo.com/video/${vimeo[1]}?title=0&byline=0`
      );
      this.wrapper.innerHTML = '';
      this.wrapper.appendChild(iframe);
      return this.wrapper;
    }

    const wrapperVideo = document.createElement('div');
    wrapperVideo.setAttribute('class', 'upload-video__wrapper');

    const video = document.createElement('video');
    video.setAttribute('src', this.data.src);
    video.setAttribute('width', '100%');
    video.setAttribute('controls', '');
    wrapperVideo.appendChild(video);
    this.wrapper.appendChild(wrapperVideo);

    return this.wrapper;
  }

  render() {
    this.wrapper = document.createElement('div');
    this.wrapper.setAttribute('class', 'video-manager');

    if (this.data && this.data.src) {
      this.wrapper.classList.add('video-manager--reset-style');
      return this.pastVideo();
    }

    this.itemLink = document.createElement('div');
    this.itemLink.innerText = this.i18n.t('Paste link');
    this.itemLink.setAttribute('class', 'video-manager__item');

    this.itemUpload = document.createElement('div');
    this.itemUpload.innerText = this.i18n.t('Upload from computer');
    this.itemUpload.setAttribute('class', 'video-manager__item');

    this.itemLink.addEventListener('click', () => {
      this.wrapper.innerHTML = '';
      this.wrapper.appendChild(
        new UploadVideoFromLink({
          api: this.api,
          config: this.config,
        }).render()
      );

      this.wrapper.classList.add('video-manager--reset-style');
    });

    this.itemUpload.addEventListener('click', () => {
      this.wrapper.innerHTML = '';
      this.wrapper.appendChild(
        new UploadVideo({
          data: {
            src: this.videoUrl,
          },
          api: this.api,
          config: this.config,
        }).render()
      );

      this.wrapper.classList.add('video-manager--reset-style');
    });

    this.wrapper.appendChild(this.itemLink);
    this.wrapper.appendChild(this.itemUpload);

    return this.wrapper;
  }

  save() {
    if (this.wrapper.querySelector('.upload-video-from-link__video')) {
      return {
        src: this.wrapper
          .querySelector('.upload-video-from-link__video')
          ?.getAttribute('src'),
      };
    }

    if (this.wrapper.querySelector('.upload-video__wrapper')) {
      return {
        src: this.wrapper
          .querySelector('.upload-video__wrapper > video')
          ?.getAttribute('src'),
      };
    }

    return {
      src: null,
    };
  }
}

export default VideoManager;
