class SelectionUtils {
  constructor() {
    this.instance = null;
    this.selection = null;
    this.savedSelectionRange = null;
    this.isFakeBackgroundEnabled = false;
    this.commandBackground = 'backColor';
    this.commandRemoveFormat = 'removeFormat';
  }

  /**
   * Return first range
   * @return {Range|null}
   */
  static get range() {
    const selection = window.getSelection();

    return selection && selection.rangeCount ? selection.getRangeAt(0) : null;
  }

  /**
   * Returns selected text as String
   * @returns {string}
   */
  static get text() {
    return window.getSelection ? window.getSelection().toString() : '';
  }

  /**
   * Returns window SelectionUtils
   * {@link https://developer.mozilla.org/ru/docs/Web/API/Window/getSelection}
   * @return {Selection}
   */
  static get() {
    return window.getSelection();
  }

  /**
   * Removes fake background
   */
  removeFakeBackground() {
    if (!this.isFakeBackgroundEnabled) {
      return;
    }

    this.isFakeBackgroundEnabled = false;
    document.execCommand(this.commandRemoveFormat);
  }

  /**
   * Sets fake background
   */
  setFakeBackground() {
    document.execCommand(this.commandBackground, false, '#d4ecff');
    this.isFakeBackgroundEnabled = true;
  }

  /**
   * Save SelectionUtils's range
   */
  save() {
    this.savedSelectionRange = SelectionUtils.range;
  }

  /**
   * Restore saved SelectionUtils's range
   */
  restore() {
    if (!this.savedSelectionRange) {
      return;
    }

    const sel = window.getSelection();

    sel.removeAllRanges();
    sel.addRange(this.savedSelectionRange);
  }

  /**
   * Clears saved selection
   */
  clearSaved() {
    this.savedSelectionRange = null;
  }

  /**
   * Collapse current selection
   */
  collapseToEnd() {
    const sel = window.getSelection();
    const range = document.createRange();

    range.selectNodeContents(sel.focusNode);
    range.collapse(false);
    sel.removeAllRanges();
    sel.addRange(range);
  }

  findParentTag(tagName, className, searchDepth = 10) {
    const selection = window.getSelection();
    let parentTag = null;

    /**
     * If selection is missing or no anchorNode or focusNode were found then return null
     */
    if (!selection || !selection.anchorNode || !selection.focusNode) {
      return null;
    }

    /**
     * Define Nodes for start and end of selection
     */
    const boundNodes = [
      /** the Node in which the selection begins */
      selection.anchorNode,
      /** the Node in which the selection ends */
      selection.focusNode,
    ];

    /**
     * For each selection parent Nodes we try to find target tag [with target class name]
     * It would be saved in parentTag variable
     */
    boundNodes.forEach((parent) => {
      /** Reset tags limit */
      let searchDepthIterable = searchDepth;

      while (searchDepthIterable > 0 && parent.parentNode) {
        /**
         * Check tag's name
         */
        if (parent.tagName === tagName) {
          /**
           * Save the result
           */
          parentTag = parent;

          /**
           * Optional additional check for class-name mismatching
           */
          if (
            className &&
            parent.classList &&
            !parent.classList.contains(className)
          ) {
            parentTag = null;
          }

          /**
           * If we have found required tag with class then go out from the cycle
           */
          if (parentTag) {
            break;
          }
        }

        /**
         * Target tag was not found. Go up to the parent and check it
         */
        parent = parent.parentNode;
        searchDepthIterable--;
      }
    });

    /**
     * Return found tag or null
     */
    return parentTag;
  }

  /**
   * Expands selection range to the passed parent node
   *
   * @param {HTMLElement} element - element which contents should be selcted
   */
  expandToTag(element) {
    const selection = window.getSelection();

    selection.removeAllRanges();
    const range = document.createRange();

    range.selectNodeContents(element);
    selection.addRange(range);
  }
}

class ScenarioInlineLink {
  static get isInline() {
    return true;
  }

  static get title() {
    return 'ScenarioInlineLink';
  }

  static get sanitize() {
    return {
      scenario: true,
    };
  }

  constructor({ api, config }) {
    this.config = config;
    this.searchFunction =
      this.config.searchFunction || (() => Promise.resolve());

    this.CSS = {
      button: 'ce-inline-tool',
      buttonActive: 'ce-inline-tool--active',
      buttonModifier: 'ce-inline-tool--link',
      buttonUnlink: 'ce-inline-tool--unlink',
      input: 'ce-inline-tool-input',
      inputShowed: 'ce-inline-tool-input--showed',
      inputWrapper: 'scenario-link__wrapper',
      inputWrapperInline: 'scenario-link__inline-wrapper',
    };

    this.nodes = {
      button: null,
      input: null,
      list: null,
      searchInput: null,
    };
    this.inputOpened = false;

    this.toolbar = api.toolbar;
    this.inlineToolbar = api.inlineToolbar;
    this.i18n = api.i18n;

    this.reset = window;
    this.selection = new SelectionUtils();
  }

  /**
   * Create button for Inline Toolbar
   */
  render() {
    this.nodes.button = document.createElement('button');
    this.nodes.button.type = 'button';
    this.nodes.button.classList.add(this.CSS.button, this.CSS.buttonModifier);
    this.nodes.button.innerHTML =
      '<svg viewBox="0 0 24 24" width="24px" height="24px" xmlns="http://www.w3.org/2000/svg"><title/><path d="M19,12a1,1,0,0,1-.55.89l-10,5A1,1,0,0,1,8,18a1,1,0,0,1-.53-.15A1,1,0,0,1,7,17V7a1,1,0,0,1,1.45-.89l10,5A1,1,0,0,1,19,12Z"/></svg>';

    return this.nodes.button;
  }

  renderActions() {
    this.nodes.input = document.createElement('div');
    this.nodes.input.classList.add(
      this.CSS.inputWrapper,
      this.CSS.inputWrapperInline
    );

    this.nodes.list = document.createElement('ul');
    this.nodes.list.setAttribute('class', 'scenario-link__list');

    this.onInput({ target: { value: '' } });

    const searchInput = document.createElement('input');
    this.searchInput = searchInput;
    searchInput.setAttribute('placeholder', this.i18n.t('Search walkthroughs'));
    searchInput.setAttribute('type', 'text');
    searchInput.setAttribute('tabindex', '-1');
    searchInput.classList.add(this.CSS.input, this.CSS.inputShowed);
    searchInput.addEventListener('input', this.onInput.bind(this));

    this.nodes.searchInput = searchInput;
    this.nodes.input.appendChild(searchInput);
    this.nodes.input.appendChild(this.nodes.list);

    return this.nodes.input;
  }

  async onInput(event) {
    const { value } = event.target;

    this.setLoadingState(true);
    const results = await this.searchFunction(value || null);
    this.setLoadingState();
    this.refreshList(results);
  }

  setLoadingState(status) {
    this.clearWrapper();

    if (status) {
      const loading = document.createElement('li');
      loading.classList.add('scenario-link__item--empty');
      loading.classList.add('scenario-link__item');
      loading.innerText = this.i18n.t('Loading');

      this.nodes.list.appendChild(loading);
    } else {
      this.clearWrapper();
    }
  }

  clearWrapper() {
    this.nodes.list.innerHTML = '';
  }

  refreshList(items = []) {
    this.clearWrapper();

    let selectedId = null;

    this.selection.restore();
    const anchorTag = this.selection.findParentTag(
      'SCENARIO',
      'hinted-scenario-link__link'
    );

    if (anchorTag) {
      selectedId = anchorTag.getAttribute('hinted-link');
    }

    if (selectedId) {
      const element = document.createElement('li');
      element.classList.add('scenario-link__item');
      element.classList.add('scenario-link__item--active');
      element.setAttribute('id', selectedId);
      element.innerText = anchorTag.getAttribute('hinted-title');
      this.nodes.list.appendChild(element);
    }

    if (!items.length) {
      const emptyItem = document.createElement('li');
      emptyItem.classList.add('scenario-link__item--empty');
      emptyItem.classList.add('scenario-link__item');
      emptyItem.innerText = this.i18n.t('There is no items');

      this.nodes.list.appendChild(emptyItem);

      this.searchInput.focus();
    }

    const filteredItems = selectedId
      ? items.filter((item) => item.id !== selectedId)
      : items;
    filteredItems.forEach((current) => {
      const { id, title } = current;

      const element = document.createElement('li');
      element.classList.add('scenario-link__item');
      element.setAttribute('id', id);
      element.innerText = title;
      element.addEventListener('click', this.onItemClick.bind(this));

      this.nodes.list.appendChild(element);
    });
  }

  surround(range) {
    if (range) {
      if (!this.inputOpened) {
        this.selection.save();
      } else {
        this.selection.restore();
      }

      const anchorTag = this.selection.findParentTag(
        'SCENARIO',
        'hinted-scenario-link__link'
      );

      if (anchorTag) {
        this.selection.expandToTag(anchorTag);
        this.unlink();
        this.closeActions();
        this.checkState();
        this.inlineToolbar.close();

        return;
      }
    }

    this.toggleActions();
  }

  checkState() {
    const anchorTag = this.selection.findParentTag(
      'SCENARIO',
      'hinted-scenario-link__link'
    );

    if (anchorTag) {
      this.nodes.button.classList.add(this.CSS.buttonUnlink);
      this.nodes.button.classList.add(this.CSS.buttonActive);

      this.openActions();

      this.selection.save();
    } else {
      this.nodes.button.classList.remove(this.CSS.buttonUnlink);
      this.nodes.button.classList.remove(this.CSS.buttonActive);
    }

    return !!anchorTag;
  }

  clear() {
    //this.closeActions();
  }

  toggleActions() {
    if (!this.inputOpened) {
      this.openActions();
    } else {
      this.closeActions();
    }
  }

  openActions() {
    this.nodes.input.classList.add('scenario-link__inline-wrapper--showed');
    this.inputOpened = true;
  }

  closeActions() {
    this.inputOpened = false;
  }

  onItemClick(event) {
    this.selection.restore();

    const id = event.target.getAttribute('id');
    const title = event.target.innerHTML;

    this.insertLink(id, title);

    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();

    this.inlineToolbar.close();
  }

  insertLink(id, title) {
    const anchorTag = this.selection.findParentTag(
      'SCENARIO',
      'hinted-scenario-link__link'
    );

    if (anchorTag) {
      anchorTag.setAttribute('hinted-link', id);
      anchorTag.setAttribute('hinted-title', title);
      anchorTag.classList.add('hinted-scenario-link__link');
    } else {
      const sel = window.getSelection();
      const range = sel.getRangeAt(0);

      const element = document.createElement('scenario');
      element.setAttribute('hinted-link', id);
      element.setAttribute('hinted-title', title);
      element.classList.add('hinted-scenario-link__link');
      element.style.textDecoration = 'underline';
      element.style.color = '#2196F3';

      element.appendChild(range.extractContents());
      range.insertNode(element);

      this.selection.expandToTag(element);
    }
  }

  unlink() {
    const termWrapper = this.selection.findParentTag(
      'SCENARIO',
      'hinted-scenario-link__link'
    );
    this.selection.expandToTag(termWrapper);

    const sel = window.getSelection();
    const range = sel.getRangeAt(0);
    const unwrappedContent = range.extractContents();
    termWrapper.parentNode.removeChild(termWrapper);
    range.insertNode(unwrappedContent);
    sel.removeAllRanges();
    sel.addRange(range);
  }
}

export default ScenarioInlineLink;
