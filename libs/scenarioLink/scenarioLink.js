class ScenarioLink {
  constructor({ data, api, config }) {
    this.config = config;
    this.searchFunction = this.config.searchFunction || (() => Promise.resolve());
    this.api = api;
    this.data = data;
    this.itemsWrapper = null;
    this.mainWrapper = null;
    this.currentId = null;
    this.currentTitle = null;
  }

  static get template() {
    return '<scenario hinted-link="xxScenarioIdxx" hinted-click="onScenarioLinkClick"><span contenteditable="false"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14" height="14" viewBox="100 100 300 300"><path d="M152.443 136.417l207.114 119.573-207.114 119.593z" fill="#000000"></path></svg>&nbsp;xxScenarioTitlexx</span></scenario>';
  }

  static getRenderedHtml({ id = '', title = '' }) {
    return ScenarioLink.template.replace('xxScenarioIdxx', id).replace('xxScenarioTitlexx', title);
  }

  static get toolbox() {
    return {
      title: 'Scenario link',
      icon:
        '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title/><path d="M19,12a1,1,0,0,1-.55.89l-10,5A1,1,0,0,1,8,18a1,1,0,0,1-.53-.15A1,1,0,0,1,7,17V7a1,1,0,0,1,1.45-.89l10,5A1,1,0,0,1,19,12Z"/></svg>',
    };
  }

  refreshList(items = []) {
    this.clearWrapper();

    if (!items.length) {
      const emptyItem = document.createElement('li');
      emptyItem.classList.add('scenario-link__item--empty');
      emptyItem.classList.add('scenario-link__item');
      emptyItem.innerText = 'There is no items';

      this.itemsWrapper.appendChild(emptyItem);
    }

    items.forEach((current) => {
      const { id, title } = current;

      const element = document.createElement('li');
      element.setAttribute('class', 'scenario-link__item');
      element.setAttribute('id', id);
      element.innerText = title;
      element.addEventListener('click', this.onItemClick.bind(this));

      this.itemsWrapper.appendChild(element);
    });
  }

  onItemClick(event) {
    const id = event.target.getAttribute('id');
    const title = event.target.innerHTML;

    this.renderResult(id, title);
  }

  renderResult(id, title) {
    this.currentId = id;
    this.currentTitle = title;
    this.mainWrapper.innerHTML = ScenarioLink.getRenderedHtml({ id, title });
  }

  clearWrapper() {
    this.itemsWrapper.innerHTML = '';
  }

  setLoadingState(status) {
    this.clearWrapper();

    if (status) {
      const loading = document.createElement('li');
      loading.classList.add('scenario-link__item--empty');
      loading.classList.add('scenario-link__item');
      loading.innerText = 'Loading';

      this.itemsWrapper.appendChild(loading);
    } else {
      this.clearWrapper();
    }
  }

  async onInput(event) {
    const { value } = event.target;

    this.setLoadingState(true);
    const results = await this.searchFunction(value || null);
    this.setLoadingState();
    this.refreshList(results);
  }

  render() {
    this.mainWrapper = document.createElement('div');
    this.mainWrapper.setAttribute('class', 'scenario-link__wrapper');

    if (this.data && Object.keys(this.data).length) {
      this.currentId = this.data.id;
      this.currentTitle = this.data.title;

      this.mainWrapper.innerHTML = ScenarioLink.getRenderedHtml({
        id: this.currentId,
        title: this.currentTitle,
      });

      return this.mainWrapper;
    }

    this.itemsWrapper = document.createElement('ul');
    this.itemsWrapper.setAttribute('class', 'scenario-link__list');

    this.onInput({ target: { value: '' } });

    const searchInput = document.createElement('input');
    searchInput.setAttribute('placeholder', 'Search scenario...');
    searchInput.setAttribute('class', 'scenario-link__input');

    searchInput.addEventListener('input', this.onInput.bind(this));

    this.mainWrapper.appendChild(searchInput);
    this.mainWrapper.appendChild(this.itemsWrapper);

    return this.mainWrapper;
  }

  save() {
    return {
      id: this.currentId,
      title: this.currentTitle,
    };
  }
}

// comment line below if you run it from example.html (debug only)
export default ScenarioLink;
