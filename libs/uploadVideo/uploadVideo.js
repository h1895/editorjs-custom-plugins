class UploadVideo {
  constructor({ data, api, config }) {
    this.data = data;
    this.api = api;
    this.i18n = api.i18n;
    this.config = config;
    this.wrapper = null;
    this.inputFile = null;
    this.videoUrl = null;
    this.label = null;
    this.error = null;
  }

  static get toolbox() {
    return {};
  }

  static get template() {
    return '<video src="xxSrcxx" width="xxWidthxx" controls></video>';
  }

  static getRenderedHtml({ width = '100%', src = '' }) {
    return UploadVideo.template
      .replace('xxSrcxx', src)
      .replace('xxWidthxx', width);
  }

  static get pasteConfig() {
    return {
      patterns: {
        mp4: /https?:\/\/.*\.mp4/gi,
        webm: /https?:\/\/.*\.webm/gi,
      },
    };
  }

  addVideo() {
    this.wrapper.innerHTML = UploadVideo.getRenderedHtml({
      src: this.videoUrl,
    });
  }

  onPaste(event) {
    this.videoUrl = event.detail.data;
    this.addVideo();
  }

  render() {
    this.wrapper = document.createElement('div');
    this.wrapper.classList.add('upload-video__wrapper');
    this.inputFile = document.createElement('input');
    this.inputFile.setAttribute('type', 'file');
    this.inputFile.setAttribute('name', 'file');
    this.inputFile.setAttribute('id', 'file');
    this.inputFile.classList.add('upload-video__input');
    this.label = document.createElement('label');
    this.label.classList.add('upload-video__label');
    this.label.setAttribute('for', 'file');
    this.label.innerHTML = `<span>${this.i18n.t('Choose file')}</span>`;

    if (this.data.src) {
      this.videoUrl = this.data.src;
      this.addVideo();
      return this.wrapper;
    }

    this.inputFile.addEventListener('input', async (event) => {
      const MAX_SIZE = 15000000;
      if (this.error && event.target.files[0].size > MAX_SIZE) {
        return;
      }

      if (this.error && event.target.files[0].size < MAX_SIZE) {
        this.wrapper.innerHTML = '';
      }

      if (event.target.files[0].size > 15000000) {
        this.error = document.createElement('span');
        this.error.innerHTML = 'File size over 15MB';
        this.error.classList.add('upload-video__error-text');
        this.label.classList.add('upload-video__error');
        this.wrapper.appendChild(this.error);
        return;
      }

      const response = await this.config.uploadVideo(event.target.files[0]);

      if (response && response.file) {
        this.videoUrl = response.file.url;
        this.addVideo();
      }
    });

    this.wrapper.appendChild(this.inputFile);
    this.wrapper.appendChild(this.label);

    return this.wrapper;
  }

  save() {
    return {
      src: this.videoUrl,
    };
  }

  getUrlVideo() {
    return this.videoUrl;
  }
}

// comment line below if you run it from example.html (debug only)
export default UploadVideo;
