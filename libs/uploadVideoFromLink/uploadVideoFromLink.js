class UploadVideoFromLink {
  constructor({ data, api, config }) {
    this.data = data;
    this.api = api;
    this.i18n = api.i18n;
    this.config = config;
    this.wrapper = null;
    this.videoUrl = null;
  }

  static get toolbox() {
    return {
      title: 'UploadVideoFromLink',
    };
  }

  static get template() {
    return '<video src="xxSrcxx" width="xxWidthxx" controls></video>';
  }

  static getRenderedHtml({ width = '100%', src = '' }) {
    return UploadVideoFromLink.template
      .replace('xxSrcxx', src)
      .replace('xxWidthxx', width);
  }

  static get pasteConfig() {
    return {
      patterns: {
        mp4: /https?:\/\/.*\.mp4/gi,
      },
    };
  }

  addVideo() {
    this.wrapper.innerHTML = UploadVideoFromLink.getRenderedHtml({
      src: this.videoUrl,
    });
  }

  render() {
    this.wrapper = document.createElement('div');
    this.wrapper.classList.add('upload-video-from-link__wrapper');

    this.inputForLink = document.createElement('input');
    this.inputForLink.classList.add('upload-video-from-link__input');
    this.inputForLink.setAttribute('placeholder', this.i18n.t('Link to video'));
    this.wrapper.appendChild(this.inputForLink);

    this.inputForLink.addEventListener('input', () => {
      this.videoUrl = this.inputForLink.value;
      const youtube = this.videoUrl.match(
        /(?:youtube\.com\/(?:[^\\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\\/ ]{11})/i
      );

      this.iframe = document.createElement('iframe');
      this.iframe.setAttribute('class', 'upload-video-from-link__video');

      if (youtube) {
        this.iframe.setAttribute(
          'src',
          `https://www.youtube.com/embed/${youtube[1]}?autoplay=1`
        );
        this.wrapper.innerHTML = '';
        this.wrapper.appendChild(this.iframe);
        return;
      }

      const vimeo = this.videoUrl.match(
        /(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\\-]+)?/i
      );

      if (vimeo) {
        this.iframe.setAttribute(
          'src',
          `https://player.vimeo.com/video/${vimeo[1]}?title=0&byline=0`
        );
        this.wrapper.innerHTML = '';
        this.wrapper.appendChild(this.iframe);
      }
    });

    return this.wrapper;
  }

  save() {
    return {
      src: this.videoUrl,
    };
  }

  getUrlVideo() {
    return this.videoUrl;
  }
}

// comment line below if you run it from example.html (debug only)
export default UploadVideoFromLink;
