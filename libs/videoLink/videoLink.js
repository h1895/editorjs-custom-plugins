class VideoLink {
  constructor({ data, api, config }) {
    this.data = data;
    this.api = api;
    this.config = config;
    this.wrapper = null;
    this.videoUrl = null;
  }

  static get template() {
    return '<video src="xxSrcxx" width="xxWidthxx" controls></video>';
  }

  static getRenderedHtml({ width = '100%', src = '' }) {
    return VideoLink.template.replace('xxSrcxx', src).replace('xxWidthxx', width);
  }

  static get pasteConfig() {
    return {
      patterns: {
        mp4: /https?:\/\/.*\.mp4/gi,
      },
    };
  }

  addVideo() {
    this.wrapper.innerHTML = VideoLink.getRenderedHtml({ src: this.videoUrl });
  }

  onPaste(event) {
    this.videoUrl = event.detail.data;
    this.addVideo();
  }

  render() {
    this.wrapper = document.createElement('div');
    this.wrapper.classList.add('video-link__wrapper');

    if (this.data && this.data.url) {
      this.videoUrl = this.data.src;
      this.addVideo();
    }

    return this.wrapper;
  }

  save() {
    return {
      src: this.videoUrl,
    };
  }
}

// comment line below if you run it from example.html (debug only)
export default VideoLink;
