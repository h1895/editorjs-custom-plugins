import Header from '@editorjs/header';
import List from '@editorjs/list';
import Paragraph from '@editorjs/paragraph';
import Embed from '@editorjs/embed';
import Quote from '@editorjs/quote';
import Delimiter from '@editorjs/delimiter';
import Underline from '@editorjs/underline';
import FontSize from 'editorjs-inline-font-size-tool';
import Image from '@editorjs/image';
import UploadVideoFromLink from './libs/uploadVideoFromLink/uploadVideoFromLink';
import VideoManager from './libs/videoManager/videoManager';

import UploadVideo from './libs/uploadVideo/uploadVideo';
import ScenarioLink from './libs/scenarioLink/scenarioLink';
import ScenarioInlineLink from './libs/scenarioLink/scenarioInlineLink';
import ScenarioStopLink from './libs/scenarioStopLink/scenarioStopLink';
import VideoLink from './libs/videoLink/videoLink';
import TextColor from './libs/textColor/textColor';

import AlignmentTuneTool from 'editorjs-text-alignment-blocktune';

export {
  ScenarioLink,
  ScenarioInlineLink,
  ScenarioStopLink,
  VideoLink,
  UploadVideoFromLink,
  VideoManager,
  UploadVideo,
  TextColor,
};

export default {
  tools: {
    header: {
      class: Header,
      tunes: ['alignmentTuneTool'],
      config: {
        levels: [1, 2, 3, 4, 5, 6],
        defaultLevel: 2,
      },
    },

    scenarioStopLink: {
      inlineToolbar: true,
      class: ScenarioStopLink,
    },

    underline: {
      class: Underline,
    },

    textColor: {
      class: TextColor,
      inlineToolbar: true,
    },

    fontSize: {
      class: FontSize,
    },

    list: {
      class: List,
      tunes: ['alignmentTuneTool'],
      inlineToolbar: true,
    },

    paragraph: {
      class: Paragraph,
      tunes: ['alignmentTuneTool'],
      config: {
        preserveBlank: true,
      },
    },

    embed: {
      class: Embed,
      config: {
        services: {
          youtube: true,
          coub: true,
          imgur: true,
          vimeo: true,
          gfycat: true,
          'twitch-video': true,
          'twitch-channel': true,
          'yandex-music-track': true,
          'yandex-music-album': true,
          'yandex-music-playlist': true,
          twitter: true,
          instagram: true,
        },
      },
    },

    quote: {
      class: Quote,
      tunes: ['alignmentTuneTool'],
      inlineToolbar: true,
      shortcut: 'CMD+SHIFT+O',
      config: {
        quotePlaceholder: 'Enter a quote',
        captionPlaceholder: "Quote's author",
      },
    },

    image: {
      class: Image,
      tunes: ['alignmentTuneTool'],
    },

    delimiter: {
      class: Delimiter,
      tunes: ['alignmentTuneTool'],
    },

    videoLink: VideoLink,

    scenarioLink: {
      class: ScenarioLink,
      tunes: ['alignmentTuneTool'],
    },

    scenarioInlineLink: {
      class: ScenarioInlineLink,
      inlineToolbar: true,
    },

    videoLink: VideoLink,

    uploadVideoFromLink: UploadVideoFromLink,

    videoManager: VideoManager,

    alignmentTuneTool: {
      class: AlignmentTuneTool,
      config: {
        default: 'left',
        blocks: {
          image: 'center',
          delimiter: 'center',
        },
      },
    },
  },
  data: {
    time: 1611059385116,

    blocks: [
      {
        type: 'header',
        data: {
          text: '',
          level: 3,
        },
      },
    ],

    version: '2.18.0',
  },
};
